import { StateOfArtPage } from './app.po';
import { browser, by, element, protractor } from 'protractor';

describe('state-of-art App', () => {
  let page: StateOfArtPage;

  beforeEach(() => {
    page = new StateOfArtPage();
  });

  it('should search for Caravaggio artworks', () => {
    page.navigateTo();
    element(by.css('input')).sendKeys('Caravaggio');
    browser.sleep( 5000 );
    element(by.css('h4')).getText().then(
      function( text ) {
        expect(text).toContain( 'Caravaggio' );
      }
    );
  });

  it('should click and redirect on detail page', () => {
    page.navigateTo();
    element(by.css('a')).click();
    browser.sleep( 5000 );
    const desc = element(by.css('.description'));
    expect(desc).toBeDefined();
  });
});
