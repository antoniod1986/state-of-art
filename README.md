# State of Art    

git clone git@bitbucket.org:antoniod1986/state-of-art.git   

cd state-of-art     

npm install   

# Open a first terminal and run the pass-through server:     
npm run server   

# Open a second terminal and run the client:     
npm run start

# Unit tests:     
npm run test

# E2E tests:     
npm run e2e
