import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ArtworkService {
  private baseUrl = environment.apiUrl;
  private key = '67CFld9n';
  private format = 'json';
  lastQuery = '';
  artworks = new BehaviorSubject( [] );

  constructor(
    private http: Http
  ) { }

  // get artworks list
  getArtworks( options: any = {} ) {
    const params = {
      'key': this.key,
      'format': this.format,
      's': 'relevance'
    };

    params['q'] = options.query || '';
    if ( params['q'] === this.lastQuery && this.lastQuery !== '' ) {
      return null;
    }
    this.lastQuery = params['q'];

    const artworkObserver = this.http.get( this.baseUrl + '/nl/collection', { params: params })
      .map(data => data.json());
    artworkObserver.subscribe(data => this.artworks.next(data.artObjects));

    return artworkObserver;
  }

  // get artwork detail
  getArtwork( id: string = '' ) {
    const params = {
      'key': this.key,
      'format': this.format
    };

    return this.http.get( this.baseUrl + `/nl/collection/${id}`, { params: params })
      .map(data => data.json());
  }
}
