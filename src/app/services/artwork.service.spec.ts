import { TestBed, inject } from '@angular/core/testing';

import { HttpModule } from '@angular/http';

import { ArtworkService } from './artwork.service';


describe('ArtworkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule
      ],
      providers: [
        ArtworkService
      ]
    });
  });

  it('should get artworks list', inject([ArtworkService], (service: ArtworkService) => {
    service.getArtworks()
      .subscribe(
      data => {
        expect( data.length ).toBeGreaterThan( 0 );
      }
    );
  }));

  it('should get artwork detail', inject([ArtworkService], (service: ArtworkService) => {
    service.getArtwork( 'NG-NM-10388-103' )
      .subscribe(
        data => {
          expect( data.artObject ).toBeDefined();
        }
      );
  }));
});
