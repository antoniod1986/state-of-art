import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { ArtworkService } from '../../services/artwork.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchInput = new FormControl();

  constructor(
    private artworkService: ArtworkService
  ) {
    this.searchInput
      .valueChanges
      .debounceTime( 1000 )
      .subscribe( query => this.search( query ) );
  }

  ngOnInit() {
    this.searchInput.patchValue( this.artworkService.lastQuery );
  }

  search( query ) {
    this.artworkService.getArtworks( {
      query: query
    });
  }
}
