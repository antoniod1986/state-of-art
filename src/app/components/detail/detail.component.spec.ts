import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Injectable } from '@angular/core';

import { DetailComponent } from './detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, Params } from '@angular/router';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';
import { Subject } from 'rxjs/Subject';

import { ArtworkService } from '../../services/artwork.service';
import { HttpModule } from '@angular/http';
import { By } from '@angular/platform-browser';

@Injectable()
export class ActivatedRouteStub {
  private subject = new BehaviorSubject(this.testParams);
  params = this.subject.asObservable();

  private _testParams: {};
  get testParams() { return this._testParams; }
  set testParams(params: {}) {
    this._testParams = params;
    this.subject.next(params);
  }
}

describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>;
  let mockActivatedRoute;
  let params: Subject<Params>;

  beforeEach(async(() => {
    params = new Subject<Params>();
    mockActivatedRoute = new ActivatedRouteStub();

    TestBed.configureTestingModule({
      declarations: [
        DetailComponent
      ],
      imports: [
        RouterTestingModule,
        HttpModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        ArtworkService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailComponent);
    component = fixture.componentInstance;

    mockActivatedRoute.testParams = { objectNumber: '3' };

    component.artwork = {
      objectNumber: 'RP-P-1904-1847',
      title: 'Gezicht op een onbekende haven aan de Zwarte Zee',
      webImage: {
        url: 'http://lh4.ggpht.com/m1TEn7QzrDVfb_UCYwyboWAeSaptbr2g5uPZvPyYayl0HjUWxx_ch5C0Z3I-6jMz4Jw2WRcfAyF9PbmMX7GCfuKkrJ0=s0'
      },
      'description': 'Gezicht op een onbekende haven aan de Zwarte Zee.',
      principalMakers: [
        {
          name: 'Hendrick van Cleve'
        }
      ],
      materials: [
        'papier'
      ],
      hasImage: true
    };

    component.serviceCalled = true;

    fixture.detectChanges();
  });

  it('should create the detail page', () => {
    expect(component).toBeTruthy();
  });

  it('should show the artwork', () => {
    const h1 = fixture.debugElement.query(By.css('h1')).nativeElement;
    const desc = fixture.debugElement.query(By.css('.description')).nativeElement;
    expect(h1).toBeDefined();
    expect(h1.innerText).toContain( 'Gezicht op een onbekende haven aan de Zwarte Zee' );
    expect(desc).toBeDefined();
    expect(desc.innerText).toContain( 'Gezicht op een onbekende haven aan de Zwarte Zee.' );
  });

  it('should NOT show the artwork', () => {
    component.artwork = {};
    fixture.detectChanges();
    const h1 = fixture.debugElement.query(By.css('h1')).nativeElement;
    expect(h1.innerText).toContain( 'Artwork not found :(' );
  });
});
