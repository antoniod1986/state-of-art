import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ArtworkService } from '../../services/artwork.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  artwork = {};
  serviceCalled = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private artworkService: ArtworkService,
    private location: Location
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      const objectNumber = params['objectNumber'] || '';
      this.artworkService.getArtwork( objectNumber )
        .subscribe( artwork => {
          this.artwork = artwork.artObject || {};
          this.serviceCalled = true;
        });
    });
  }

  backClicked() {
    this.location.back();
  }
}
