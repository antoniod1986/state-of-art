import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemComponent } from './item.component';
import { Injectable } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, Params } from '@angular/router';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';
import { Subject } from 'rxjs/Subject';

import { ArtworkService } from '../../services/artwork.service';
import { HttpModule } from '@angular/http';
import { By } from '@angular/platform-browser';

@Injectable()
export class ActivatedRouteStub {
  private subject = new BehaviorSubject(this.testParams);
  params = this.subject.asObservable();

  private _testParams: {};
  get testParams() { return this._testParams; }
  set testParams(params: {}) {
    this._testParams = params;
    this.subject.next(params);
  }
}

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;
  let mockActivatedRoute;
  let params: Subject<Params>;

  beforeEach(async(() => {
    params = new Subject<Params>();
    mockActivatedRoute = new ActivatedRouteStub();

    TestBed.configureTestingModule({
      declarations: [
        ItemComponent
      ],
      imports: [
        RouterTestingModule,
        HttpModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        ArtworkService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;

    mockActivatedRoute.testParams = { objectNumber: '3' };

    component.artwork = {
      objectNumber: 'RP-P-1904-1847',
      title: 'Gezicht op een onbekende haven aan de Zwarte Zee',
      webImage: {
        url: 'http://lh4.ggpht.com/m1TEn7QzrDVfb_UCYwyboWAeSaptbr2g5uPZvPyYayl0HjUWxx_ch5C0Z3I-6jMz4Jw2WRcfAyF9PbmMX7GCfuKkrJ0=s0'
      },
      'description': 'Gezicht op een onbekende haven aan de Zwarte Zee.',
      principalMakers: [
        {
          name: 'Hendrick van Cleve'
        }
      ],
      materials: [
        'papier'
      ],
      hasImage: true
    };

    fixture.detectChanges();
  });

  it('should create item component', () => {
    expect(component).toBeTruthy();
  });

  it('should show the item', () => {
    const h4 = fixture.debugElement.query(By.css('h4')).nativeElement;
    const img = fixture.debugElement.query(By.css('img')).nativeElement;
    expect(h4).toBeDefined();
    expect(h4.innerText).toContain( 'Gezicht op een onbekende haven aan de Zwarte Zee' );
    expect(img).toBeDefined();
  });
});
