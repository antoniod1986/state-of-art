import { Component, OnInit } from '@angular/core';
import { ArtworkService } from '../../services/artwork.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  artworks = [];

  constructor(
    private artworkService: ArtworkService
  ) { }

  ngOnInit() {
    this.artworkService.artworks
      .subscribe( artworks => this.artworks = artworks );
  }

}
