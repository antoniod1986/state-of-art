import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageComponent } from './homepage.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchComponent } from '../search/search.component';
import { ListComponent } from '../list/list.component';
import { ItemComponent } from '../item/item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArtworkService } from '../../services/artwork.service';
import { HttpModule } from '@angular/http';
import { By } from '@angular/platform-browser';

describe('HomepageComponent', () => {
  let component: HomepageComponent;
  let fixture: ComponentFixture<HomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomepageComponent,
        SearchComponent,
        ListComponent,
        ItemComponent
      ],
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule
      ],
      providers: [
        ArtworkService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create homepage', () => {
    expect(component).toBeTruthy();
  });

  it('should show search input', () => {
    const input = fixture.debugElement.query(By.css('input'));
    const el = input.nativeElement;
    expect(el).toBeDefined();
  });
});
